import { Injectable } from '@angular/core';
import { Receta } from './receta.model';

@Injectable({  
  providedIn: 'root'
})
export class RecetasService {
    recetas: Receta[] = [  
     {id:1, titulo:'Pizza', imageUrl:'https://www.dondeir.com/wp-content/uploads/2019/08/pizza-hut-cadenas-de-pizza-cdmx.jpg',    ingredientes: ['pan','queso', 'tomate', 'peperoni']},
      {id: 2, titulo:'Tacos',    imageUrl:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCy_L2Fh1RcVLFGtjzT-_vfxzhMopWktzECA&usqp=CAU',    ingredientes:['carne','tortilla']},
      {id: 3, titulo:'Hamburguesa', imageUrl:'https://mx.hola.com/imagenes/cocina/recetas/2014051971376/hamburguesa-americana/0-270-627/hamburguesa_americana_1-m.jpg', ingredientes: ['pan','carne', 'jamon','queso','tomate','aguacate']},
  ]; 
 constructor() { }  
getAllRecetas(){    
  return [...this.recetas];  
}  
getReceta(recetaId:number){ 
     return {...this.recetas.find(r => {
          return r.id === recetaId;   
   })};  
}
deleteReceta(recetaId: number){
    this.recetas = this.recetas.filter(receta=> {
      return receta.id !==recetaId;
    });
}

}